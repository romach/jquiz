package com.jquiz.form;

import com.jquiz.form.QuizForm;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QuizFormTest {

    /** form common */

    @Test
    public void emptyForm() {
        QuizForm form = QuizForm.emptyForm();
        form.validate();

        assertTrue(form.hasErrors());
        assertTrue(form.hasError("quizTitle"));
        assertTrue(form.hasError("quizDescription"));
        assertTrue(form.hasError("passingScore"));
    }

    @Test
    public void formOk() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("quizTitle", "Objects");
        form.setValue("quizDescription", "Quiz about objects");
        form.setValue("passingScore", "70");
        form.validate();

        assertFalse(form.hasErrors());
    }

    /** quizTitle */

    @Test
    public void quizTitleOk() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("quizTitle", "Objects");
        form.validate();

        assertFalse(form.hasError("quizTitle"));
    }

    @Test
    public void quizTitleEmpty() {
        QuizForm form = QuizForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("quizTitle"));
        assertEquals(form.getError("quizTitle"), "Required");
    }

    @Test
    public void quizTitleIncorrectLength() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("quizTitle", "tyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("quizTitle"));
        assertEquals(form.getError("quizTitle"), "Length is too big");
    }

    /** quizDescription */

    @Test
    public void quizDescriptionOk() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("quizDescription", "Quiz about Objects");
        form.validate();

        assertFalse(form.hasError("quizDescription"));
    }

    @Test
    public void quizDescriptionEmpty() {
        QuizForm form = QuizForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("quizDescription"));
        assertEquals(form.getError("quizDescription"), "Required");
    }

    @Test
    public void quizDescriptionIncorrectLength() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("quizDescription", "tyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop");
        form.validate();

        assertTrue(form.hasError("quizDescription"));
        assertEquals(form.getError("quizDescription"), "Length is too big");
    }

    /** passingScore */

    @Test
    public void passingScoreOk() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("passingScore", "50");
        form.validate();

        assertFalse(form.hasError("passingScore"));
    }

    @Test
    public void passingScoreEmpty() {
        QuizForm form = QuizForm.emptyForm();
        form.validate();

        assertTrue(form.hasError("passingScore"));
        assertEquals(form.getError("passingScore"), "Required");
    }

    @Test
    public void passingScoreSmall() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("passingScore", "0");
        form.validate();

        assertTrue(form.hasError("passingScore"));
        assertEquals(form.getError("passingScore"), "Should be in range [1 - 100]");
    }

    @Test
    public void passingScoreLarge() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("passingScore", "101");
        form.validate();

        assertTrue(form.hasError("passingScore"));
        assertEquals(form.getError("passingScore"), "Should be in range [1 - 100]");
    }

    @Test
    public void passingScoreWrongFormat() {
        QuizForm form = QuizForm.emptyForm();
        form.setValue("passingScore", "ee");
        form.validate();

        assertTrue(form.hasError("passingScore"));
        assertEquals(form.getError("passingScore"), "Wrong format");
    }
}