package com.jquiz.service;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Database Connection
 */
public class DatabaseConnection {
    
    protected static DatabaseConnection instance;
    protected Connection con;

    protected DatabaseConnection() {}
    
    public static synchronized DatabaseConnection getInstance() throws SQLException {
        if (instance == null) {
            try {
                instance = new DatabaseConnection();
                InitialContext ic = new InitialContext();
                Context xmlContext = (Context) ic.lookup("java:/comp/env");
                DataSource dataSource = (DataSource) xmlContext.lookup("jdbc/jquiz");
                instance.con = dataSource.getConnection();
            } catch (Exception e) {
                throw new SQLException(e.getMessage());
            }
        }
        return instance;
    }

    public Connection getConnection() {
        return con;
    }
    
}
