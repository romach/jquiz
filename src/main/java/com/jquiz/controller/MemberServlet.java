package com.jquiz.controller;

import com.jquiz.form.LoginForm;
import com.jquiz.form.RegisterForm;
import com.jquiz.model.Member;
import com.jquiz.service.Auth;
import com.jquiz.service.Router;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class MemberServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        String action = Router.getActionName(req);
        System.out.println("Action: " + action);
        switch (action) {
            // User go to the "Login" link
            case "login":
                loginForm(req, resp);
                break;

            case "logout":
                logout(req, resp);
                break;

            case "register":
                registerForm(req, resp);
                break;

            case "profile":
                profilePage(req, resp);
                break;

            case "list":
                try {
                    listMembers(req, resp);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String action = Router.getActionName(req);
        switch (action) {
            // User click on "Login" button
            case "login":
                processLogin(req, resp);
                break;

            case "register":
                processRegister(req, resp);
                break;

            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        
    }
    
    /** Actions */
    
    protected void loginForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }
        
        req.setAttribute("page_title", "Login");
        req.setAttribute("form", LoginForm.emptyForm());
        req.getRequestDispatcher("/memberLoginForm.jsp").forward(req, resp);
    }

    protected void processLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }
        
        LoginForm form = LoginForm.fromRequest(req);
        form.validate();
        if (!form.hasErrors()) {
            Member member;
            try {
                member = Member.findMemberByUsername(form.getValue("username"));
                if (member.passwordSecured.equals(DigestUtils.md5Hex(form.getValue("password")))) {
                    Auth.createAuth(member, session.getId());
                    String contextPath = req.getContextPath();
                    resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
                    return;
                }
            } catch (Exception e) {}
            form.setError("general", "Username or Password Incorrect");
        } else {
            form.setError("general", "Error occurred");
        }

        // If errors were occurred
        req.setAttribute("page_title", "Login");
        req.setAttribute("form", form);
        req.getRequestDispatcher("/memberLoginForm.jsp").forward(req, resp);
    }

    protected void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException  {
        HttpSession session = req.getSession();
        Auth.destroyAuth(session.getId());
        String contextPath = req.getContextPath();
        resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
    }

    protected void registerForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }

        req.setAttribute("page_title", "Register");
        req.setAttribute("form", RegisterForm.emptyForm());
        req.getRequestDispatcher("/memberRegisterForm.jsp").forward(req, resp);
    }

    protected void processRegister(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }

        RegisterForm form = RegisterForm.fromRequest(req);
        form.validate();

        Member member;
        try {
            member = Member.findMemberByUsername(form.getValue("username"));
            if (member != null) {
                form.setError("username", "Member with this Username already exists");
            }
        } catch (Exception e) {}


        try {
            member = Member.findMemberByEmail(form.getValue("email"));
            if (member != null) {
                form.setError("email", "Member with this Email already exists");
            }
        } catch (Exception e) {}

        if (!form.hasErrors()) {
            try{
                member = new Member();
                member.username = form.getValue("username");
                member.email = form.getValue("email");
                member.firstName = form.getValue("firstName");
                member.lastName = form.getValue("lastName");
                member.passwordPlain = form.getValue("password");
                member.save();

                Auth.createAuth(member, session.getId());
                String contextPath = req.getContextPath();
                resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
                return;
            } catch (Exception e) {
                form.setError("general", e.getMessage());
            }
        } else {
            form.setError("general", "Error occurred");
        }

        // If errors were occurred
        req.setAttribute("page_title", "Register");
        req.setAttribute("form", form);
        req.getRequestDispatcher("/memberRegisterForm.jsp").forward(req, resp);
    }

    protected void profilePage(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        throw new ServletException("TODO: Implement me");
    }

    protected void listMembers(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, SQLException {

        final ArrayList<Member> members = Member.findAll();
        req.setAttribute("page_title", "TOP Members");
        req.setAttribute("members", members);
        req.getRequestDispatcher("/membersList.jsp").forward(req, resp);
    }
    
}