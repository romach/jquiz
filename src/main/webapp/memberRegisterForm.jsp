<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="form" scope="request" type="com.jquiz.form.RegisterForm"/>
<jsp:include page="_header.jsp" />
    <div class="container">
        <form action="<c:out value="${base_url}"/>member/register" method="post" class="form-signin">
            <% if (form.hasError("general")) { %>
                <div class="alert alert-danger">${fn:escapeXml(form.getError("general"))}</div>
            <% } %>
            <h2 class="form-signin-heading">Register</h2>

            <div class="form-group<% if (form.hasError("username")) { %> has-error<% } %>">
                <input type="text" id="inputUsername" class="form-control" placeholder="Username" required autofocus maxlength="30" name="username" value="${fn:escapeXml(form.getValue("username"))}">
                <p class="help-block">${fn:escapeXml(form.getError("username"))}</p>
            </div>

            <div class="form-group<% if (form.hasError("email")) { %> has-error<% } %>">
                <input type="text" id="inputEmail" class="form-control" placeholder="Email" required autofocus maxlength="60" name="email" value="${fn:escapeXml(form.getValue("email"))}">
                <p class="help-block">${fn:escapeXml(form.getError("email"))}</p>
            </div>

            <div class="form-group<% if (form.hasError("password")) { %> has-error<% } %>">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required maxlength="30" name="password" value="">
                <p class="help-block">${fn:escapeXml(form.getError("password"))}</p>
            </div>

            <div class="form-group<% if (form.hasError("passwordConfirm")) { %> has-error<% } %>">
                <input type="password" id="inputPasswordConfirm" class="form-control" placeholder="Password confirm" required maxlength="30" name="passwordConfirm" value="">
                <p class="help-block">${fn:escapeXml(form.getError("passwordConfirm"))}</p>
            </div>

            <div class="form-group<% if (form.hasError("firstName")) { %> has-error<% } %>">
                <input type="text" id="inputFirstName" class="form-control" placeholder="First Name" required autofocus maxlength="30" name="firstName" value="${fn:escapeXml(form.getValue("firstName"))}">
                <p class="help-block">${fn:escapeXml(form.getError("firstName"))}</p>
            </div>

            <div class="form-group<% if (form.hasError("lastName")) { %> has-error<% } %>">
                <input type="text" id="inputLastName" class="form-control" placeholder="Last Name" required autofocus maxlength="30" name="lastName" value="${fn:escapeXml(form.getValue("lastName"))}">
                <p class="help-block">${fn:escapeXml(form.getError("lastName"))}</p>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </form>
    </div>
<jsp:include page="_footer.jsp" />